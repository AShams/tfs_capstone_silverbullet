// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Jam.h"
#include "JamPawn.h"
#include "JamProjectile.h"
#include "TimerManager.h"

const FName AJamPawn::MoveForwardBinding("MoveForward");
const FName AJamPawn::MoveRightBinding("MoveRight");
const FName AJamPawn::FireForwardBinding("FireForward");
const FName AJamPawn::FireRightBinding("FireRight");

AJamPawn::AJamPawn()
{	
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ShipMesh(TEXT("/Game/TwinStick/Meshes/TwinStickUFO.TwinStickUFO"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CUBE(TEXT("/Engine/BasicShapes/Cube.Cube"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> NOTCUBE(TEXT("/Engine/BasicShapes/Cylinder.Cylinder"));
	 
	// Create the mesh component
	ShipMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShipMesh"));
	RootComponent = ShipMeshComponent;
	ShipMeshComponent->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);
	ShipMeshComponent->SetStaticMesh(ShipMesh.Object);
	
	// Cache our sound effect
	static ConstructorHelpers::FObjectFinder<USoundBase> FireAudio(TEXT("/Game/TwinStick/Audio/TwinStickFire.TwinStickFire"));
	FireSound = FireAudio.Object;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->AttachTo(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when ship does
	CameraBoom->TargetArmLength = 1200.f;
	CameraBoom->RelativeRotation = FRotator(-80.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	CameraComponent->AttachTo(CameraBoom, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false;	// Camera does not rotate relative to arm

	CubeComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("THE CUBE!"));
	CubeComponent->AttachTo(RootComponent);
	//CubeComponent->SetMaterial();
	//CubeComponent->SetStaticMesh(CUBE.Object);
	//CubeComponent->SetStaticMesh(NOTCUBE.Object);
	CubeComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	
	
	// Movement
	MoveSpeed = 1000.0f;
	// Weapon
	GunOffset = FVector(90.f, 0.f, 0.f);
	FireRate = 0.1f;
	bCanFire = true;
}

void AJamPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	check(InputComponent);

	// set up gameplay key bindings
	InputComponent->BindAxis(MoveForwardBinding);
	InputComponent->BindAxis(MoveRightBinding);
	InputComponent->BindAxis(FireForwardBinding);
	InputComponent->BindAxis(FireRightBinding);


	//InputComponent->BindAction(ShootTheGun, IE_Pressed, this, AJamPawn::FireShot);
	//InputComponent->BindAction("Fire", IE_Released, this, AJamPawn::FireShot);
}

void AJamPawn::Tick(float DeltaSeconds)
{
	// Find movement direction
	const float ForwardValue = GetInputAxisValue(MoveForwardBinding);
	const float RightValue = GetInputAxisValue(MoveRightBinding);

	// Clamp max size so that (X=1, Y=1) doesn't cause faster movement in diagonal directions
	const FVector MoveDirection = FVector(ForwardValue, RightValue, 0.f).GetClampedToMaxSize(1.0f);

	// Calculate  movement
	const FVector Movement = MoveDirection * MoveSpeed * DeltaSeconds;

	// Two FVectors are created. mouseLocation and mouseDirection. FVector is very similar to a Vector3 from unity3d.
	// FVectors store an X, Y, and Z component.
	FVector mouseLocation, mouseDirection;
	APlayerController* playerController = (APlayerController*)GetWorld()->GetFirstPlayerController();

	// The mouse's location and direction are stored in FVector, since the goal is to make
	// the pawn face toward the mouse, the mouse direction is used 
	playerController->DeprojectMousePositionToWorld(mouseLocation, mouseDirection);

	// FRotator stores 3 values, Pitch, Yaw, and Roll.
	// FRotator requires 3 floats. Pitch and Roll are not altered.
	// Comment out this line if you prefer method 2.
	// ---Method 1---
	FRotator temp = FRotator(0.0f, mouseDirection.Rotation().Yaw, 0.0f);

	// Other method of altering temp's values. Stil works, in my opinion a little less clean to read.
	// Highlight the block of commented code and press Ctrl+K+U to uncomment the block of highlighted code.
	// Ctrl+K+C comments out the highlighted lines of code.

	// ---Method 2---
	//FRotator temp;
	//temp.Yaw = mouseDirection.Rotation().Yaw;
	//temp.Roll = 0.0f;
	//temp.Pitch = 0.0f;
	

	// If non-zero size, move this actor
	if (Movement.SizeSquared() > 0.0f)
	{
		const FRotator NewRotation = Movement.Rotation();
		FHitResult Hit(1.f);
		
		//RootComponent->MoveComponent(Movement, NewRotation, true, &Hit );
		
		if (Hit.IsValidBlockingHit())
		{
			const FVector Normal2D = Hit.Normal.GetSafeNormal2D();
			const FVector Deflection = FVector::VectorPlaneProject(Movement, Normal2D) * (1.f - Hit.Time);
			//RootComponent->MoveComponent(Deflection, NewRotation, true);

			
		}
	}
	
	// If temp's Yaw is not 0.0f, MoveComponent is called used temp as a rotator, instead of Movement.Rotation.
	// This makes the pawn default to facing it's current movement direction if the mouse cursor isn't in the game window.
	// Not entirely practical for the kind of game we're making, just an observation.
	if (temp.Yaw != 0.0f)
		RootComponent->MoveComponent(Movement, temp, true);
	else
	{
		RootComponent->MoveComponent(Movement, Movement.Rotation(),true);

	}


	// Create fire direction vector
	const float FireForwardValue = GetInputAxisValue(FireForwardBinding);
	const float FireRightValue = GetInputAxisValue(FireRightBinding);
	const FVector FireDirection = FVector(FireForwardValue, FireRightValue, 0.f);

	
	
	// Try and fire a shot
	FireShot(FireDirection);
}

void AJamPawn::FireShot(FVector FireDirection)
{
	// If we it's ok to fire again
	if (bCanFire == true)
	{
		// If we are pressing fire stick in a direction
		if (FireDirection.SizeSquared() > 0.0f)
		{
			const FRotator FireRotation = FireDirection.Rotation();
			// Spawn projectile at an offset from this pawn
			const FVector SpawnLocation = GetActorLocation() + FireRotation.RotateVector(GunOffset);

			UWorld* const World = GetWorld();
			if (World != NULL)
			{
				// spawn the projectile
				World->SpawnActor<AJamProjectile>(SpawnLocation, FireRotation);
			}

			bCanFire = false;
			World->GetTimerManager().SetTimer(TimerHandle_ShotTimerExpired, this, &AJamPawn::ShotTimerExpired, FireRate);

			// try and play the sound if specified
			if (FireSound != nullptr)
			{
				UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
			}

			bCanFire = false;
		}
	}
}

void AJamPawn::ShotTimerExpired()
{
	bCanFire = true;
}

