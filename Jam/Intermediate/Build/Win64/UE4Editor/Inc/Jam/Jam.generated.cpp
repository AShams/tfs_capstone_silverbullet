// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Boilerplate C++ definitions for a single module.
	This is automatically generated by UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "Jam.h"
#include "Jam.generated.dep.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeJam() {}
	void AJamGameMode::StaticRegisterNativesAJamGameMode()
	{
	}
	IMPLEMENT_CLASS(AJamGameMode, 2449672125);
	void AJamPawn::StaticRegisterNativesAJamPawn()
	{
	}
	IMPLEMENT_CLASS(AJamPawn, 2058514944);
	void AJamProjectile::StaticRegisterNativesAJamProjectile()
	{
		FNativeFunctionRegistrar::RegisterFunction(AJamProjectile::StaticClass(),"OnHit",(Native)&AJamProjectile::execOnHit);
	}
	IMPLEMENT_CLASS(AJamProjectile, 3340367092);
#if USE_COMPILED_IN_NATIVES
// Cross Module References
	ENGINE_API class UClass* Z_Construct_UClass_AGameMode();
	ENGINE_API class UClass* Z_Construct_UClass_APawn();
	ENGINE_API class UClass* Z_Construct_UClass_USoundBase_NoRegister();
	COREUOBJECT_API class UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API class UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UMaterial_NoRegister();
	ENGINE_API class UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	ENGINE_API class UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_AActor();
	ENGINE_API class UClass* Z_Construct_UClass_UProjectileMovementComponent_NoRegister();

	JAM_API class UClass* Z_Construct_UClass_AJamGameMode_NoRegister();
	JAM_API class UClass* Z_Construct_UClass_AJamGameMode();
	JAM_API class UClass* Z_Construct_UClass_AJamPawn_NoRegister();
	JAM_API class UClass* Z_Construct_UClass_AJamPawn();
	JAM_API class UFunction* Z_Construct_UFunction_AJamProjectile_OnHit();
	JAM_API class UClass* Z_Construct_UClass_AJamProjectile_NoRegister();
	JAM_API class UClass* Z_Construct_UClass_AJamProjectile();
	JAM_API class UPackage* Z_Construct_UPackage_Jam();
	UClass* Z_Construct_UClass_AJamGameMode_NoRegister()
	{
		return AJamGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_AJamGameMode()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AGameMode();
			Z_Construct_UPackage_Jam();
			OuterClass = AJamGameMode::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x2088028C;


				OuterClass->ClassConfigName = FName(TEXT("Game"));
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("JamGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("JamGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AJamGameMode(Z_Construct_UClass_AJamGameMode, TEXT("AJamGameMode"));
	DEFINE_VTABLE_PTR_HELPER_CTOR(AJamGameMode);
	UClass* Z_Construct_UClass_AJamPawn_NoRegister()
	{
		return AJamPawn::StaticClass();
	}
	UClass* Z_Construct_UClass_AJamPawn()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_APawn();
			Z_Construct_UPackage_Jam();
			OuterClass = AJamPawn::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20800080;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_FireSound = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("FireSound"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(FireSound, AJamPawn), 0x0000000000000005, Z_Construct_UClass_USoundBase_NoRegister());
				UProperty* NewProp_MoveSpeed = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MoveSpeed"), RF_Public|RF_Transient|RF_Native) UFloatProperty(CPP_PROPERTY_BASE(MoveSpeed, AJamPawn), 0x0000000000000005);
				UProperty* NewProp_FireRate = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("FireRate"), RF_Public|RF_Transient|RF_Native) UFloatProperty(CPP_PROPERTY_BASE(FireRate, AJamPawn), 0x0000000000000005);
				UProperty* NewProp_GunOffset = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("GunOffset"), RF_Public|RF_Transient|RF_Native) UStructProperty(CPP_PROPERTY_BASE(GunOffset, AJamPawn), 0x0000000000000005, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_CubeComponent = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CubeComponent"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(CubeComponent, AJamPawn), 0x00000000000b001d, Z_Construct_UClass_UStaticMeshComponent_NoRegister());
				UProperty* NewProp_CameraBoom = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CameraBoom"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(CameraBoom, AJamPawn), 0x00000000000a001d, Z_Construct_UClass_USpringArmComponent_NoRegister());
				UProperty* NewProp_CameraComponent = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CameraComponent"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(CameraComponent, AJamPawn), 0x00000000000a001d, Z_Construct_UClass_UCameraComponent_NoRegister());
				UProperty* NewProp_CubeMesh = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CubeMesh"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(CubeMesh, AJamPawn), 0x0000000000030015, Z_Construct_UClass_UMaterial_NoRegister());
				UProperty* NewProp_ShipMeshComponent = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ShipMeshComponent"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(ShipMeshComponent, AJamPawn), 0x00000000000b001d, Z_Construct_UClass_UStaticMeshComponent_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintType"), TEXT("true"));
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("JamPawn.h"));
				MetaData->SetValue(OuterClass, TEXT("IsBlueprintBase"), TEXT("true"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("JamPawn.h"));
				MetaData->SetValue(NewProp_FireSound, TEXT("Category"), TEXT("Audio"));
				MetaData->SetValue(NewProp_FireSound, TEXT("ModuleRelativePath"), TEXT("JamPawn.h"));
				MetaData->SetValue(NewProp_FireSound, TEXT("ToolTip"), TEXT("Sound to play each time we fire"));
				MetaData->SetValue(NewProp_MoveSpeed, TEXT("Category"), TEXT("Gameplay"));
				MetaData->SetValue(NewProp_MoveSpeed, TEXT("ModuleRelativePath"), TEXT("JamPawn.h"));
				MetaData->SetValue(NewProp_MoveSpeed, TEXT("ToolTip"), TEXT("The speed our ship moves around the level"));
				MetaData->SetValue(NewProp_FireRate, TEXT("Category"), TEXT("Gameplay"));
				MetaData->SetValue(NewProp_FireRate, TEXT("ModuleRelativePath"), TEXT("JamPawn.h"));
				MetaData->SetValue(NewProp_FireRate, TEXT("ToolTip"), TEXT("How fast the weapon will fire"));
				MetaData->SetValue(NewProp_GunOffset, TEXT("Category"), TEXT("Gameplay"));
				MetaData->SetValue(NewProp_GunOffset, TEXT("ModuleRelativePath"), TEXT("JamPawn.h"));
				MetaData->SetValue(NewProp_GunOffset, TEXT("ToolTip"), TEXT("Offset from the ships location to spawn projectiles"));
				MetaData->SetValue(NewProp_CubeComponent, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_CubeComponent, TEXT("Category"), TEXT("Primitive"));
				MetaData->SetValue(NewProp_CubeComponent, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_CubeComponent, TEXT("ModuleRelativePath"), TEXT("JamPawn.h"));
				MetaData->SetValue(NewProp_CameraBoom, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_CameraBoom, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_CameraBoom, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_CameraBoom, TEXT("ModuleRelativePath"), TEXT("JamPawn.h"));
				MetaData->SetValue(NewProp_CameraBoom, TEXT("ToolTip"), TEXT("Camera boom positioning the camera above the character"));
				MetaData->SetValue(NewProp_CameraComponent, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_CameraComponent, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_CameraComponent, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_CameraComponent, TEXT("ModuleRelativePath"), TEXT("JamPawn.h"));
				MetaData->SetValue(NewProp_CameraComponent, TEXT("ToolTip"), TEXT("The camera"));
				MetaData->SetValue(NewProp_CubeMesh, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_CubeMesh, TEXT("Category"), TEXT("Mesh"));
				MetaData->SetValue(NewProp_CubeMesh, TEXT("ModuleRelativePath"), TEXT("JamPawn.h"));
				MetaData->SetValue(NewProp_ShipMeshComponent, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_ShipMeshComponent, TEXT("Category"), TEXT("Mesh"));
				MetaData->SetValue(NewProp_ShipMeshComponent, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_ShipMeshComponent, TEXT("ModuleRelativePath"), TEXT("JamPawn.h"));
				MetaData->SetValue(NewProp_ShipMeshComponent, TEXT("ToolTip"), TEXT("The mesh component"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AJamPawn(Z_Construct_UClass_AJamPawn, TEXT("AJamPawn"));
	DEFINE_VTABLE_PTR_HELPER_CTOR(AJamPawn);
	UFunction* Z_Construct_UFunction_AJamProjectile_OnHit()
	{
		struct JamProjectile_eventOnHit_Parms
		{
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			FVector NormalImpulse;
			FHitResult Hit;
		};
		UObject* Outer=Z_Construct_UClass_AJamProjectile();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnHit"), RF_Public|RF_Transient|RF_Native) UFunction(FObjectInitializer(), NULL, 0x00C20401, 65535, sizeof(JamProjectile_eventOnHit_Parms));
			UProperty* NewProp_Hit = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Hit"), RF_Public|RF_Transient|RF_Native) UStructProperty(CPP_PROPERTY_BASE(Hit, JamProjectile_eventOnHit_Parms), 0x0000008008000182, Z_Construct_UScriptStruct_FHitResult());
			UProperty* NewProp_NormalImpulse = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("NormalImpulse"), RF_Public|RF_Transient|RF_Native) UStructProperty(CPP_PROPERTY_BASE(NormalImpulse, JamProjectile_eventOnHit_Parms), 0x0000000000000080, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_OtherComp = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherComp"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(OtherComp, JamProjectile_eventOnHit_Parms), 0x0000000000080080, Z_Construct_UClass_UPrimitiveComponent_NoRegister());
			UProperty* NewProp_OtherActor = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherActor"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(OtherActor, JamProjectile_eventOnHit_Parms), 0x0000000000000080, Z_Construct_UClass_AActor_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("JamProjectile.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Function to handle the projectile hitting something"));
			MetaData->SetValue(NewProp_OtherComp, TEXT("EditInline"), TEXT("true"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AJamProjectile_NoRegister()
	{
		return AJamProjectile::StaticClass();
	}
	UClass* Z_Construct_UClass_AJamProjectile()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage_Jam();
			OuterClass = AJamProjectile::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20800080;

				OuterClass->LinkChild(Z_Construct_UFunction_AJamProjectile_OnHit());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_ProjectileMovement = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ProjectileMovement"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(ProjectileMovement, AJamProjectile), 0x00000000000a001d, Z_Construct_UClass_UProjectileMovementComponent_NoRegister());
				UProperty* NewProp_ProjectileMesh = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ProjectileMesh"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(ProjectileMesh, AJamProjectile), 0x00000000000a001d, Z_Construct_UClass_UStaticMeshComponent_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMap(Z_Construct_UFunction_AJamProjectile_OnHit()); // 2994123089
				OuterClass->ClassConfigName = FName(TEXT("Game"));
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("JamProjectile.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("JamProjectile.h"));
				MetaData->SetValue(NewProp_ProjectileMovement, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_ProjectileMovement, TEXT("Category"), TEXT("Movement"));
				MetaData->SetValue(NewProp_ProjectileMovement, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_ProjectileMovement, TEXT("ModuleRelativePath"), TEXT("JamProjectile.h"));
				MetaData->SetValue(NewProp_ProjectileMovement, TEXT("ToolTip"), TEXT("Projectile movement component"));
				MetaData->SetValue(NewProp_ProjectileMesh, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_ProjectileMesh, TEXT("Category"), TEXT("Projectile"));
				MetaData->SetValue(NewProp_ProjectileMesh, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_ProjectileMesh, TEXT("ModuleRelativePath"), TEXT("JamProjectile.h"));
				MetaData->SetValue(NewProp_ProjectileMesh, TEXT("ToolTip"), TEXT("Sphere collision component"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AJamProjectile(Z_Construct_UClass_AJamProjectile, TEXT("AJamProjectile"));
	DEFINE_VTABLE_PTR_HELPER_CTOR(AJamProjectile);
	UPackage* Z_Construct_UPackage_Jam()
	{
		static UPackage* ReturnPackage = NULL;
		if (!ReturnPackage)
		{
			ReturnPackage = CastChecked<UPackage>(StaticFindObjectFast(UPackage::StaticClass(), NULL, FName(TEXT("/Script/Jam")), false, false));
			ReturnPackage->PackageFlags |= PKG_CompiledIn | 0x00000000;
			FGuid Guid;
			Guid.A = 0x262A1CB4;
			Guid.B = 0x9993003C;
			Guid.C = 0x00000000;
			Guid.D = 0x00000000;
			ReturnPackage->SetGuid(Guid);

		}
		return ReturnPackage;
	}
#endif

PRAGMA_ENABLE_DEPRECATION_WARNINGS
