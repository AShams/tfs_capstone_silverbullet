// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Jam.h"
#include "JamGameMode.h"
#include "JamPawn.h"

AJamGameMode::AJamGameMode()
{
	// set default pawn class to our character class
	DefaultPawnClass = AJamPawn::StaticClass();
}

