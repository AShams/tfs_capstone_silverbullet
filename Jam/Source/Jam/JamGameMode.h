// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "GameFramework/GameMode.h"
#include "JamGameMode.generated.h"


UCLASS(minimalapi)
class AJamGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AJamGameMode();
};



