// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Jam.h"


IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Jam, "Jam" );

DEFINE_LOG_CATEGORY(LogJam)
 